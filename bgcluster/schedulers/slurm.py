import csv
import logging
import subprocess

from collections import defaultdict

logger = logging.getLogger(__file__)

SLURM_STATUS_CONVERSION = {"Done": ['COMPLETED', 'CD'],
                           "Error": ['FAILED', 'F', 'CANCELLED', 'CA', 'TIMEOUT', 'TO', 'PREEMPTED', 'PR',
                                              'BOOT_FAIL', 'BF', 'NODE_FAIL', 'NF', 'DEADLINE', 'REVOKED',
                                              'SPECIAL_EXIT', 'SE'],
                           "Running": ['RUNNING', 'R', 'COMPLETING', 'CG'],
                           "Pending": ['PENDING', 'PD', 'CONFIGURING', 'CF', 'SUSPENDED', 'S', 'RESIZING',
                                               'STOPPED', 'ST'],
                           }

SLURM_STATUS = {}
for k, v in SLURM_STATUS_CONVERSION.items():
    for i in v:
        SLURM_STATUS[i] = k


def execute_command(cmd):
    try:
        out = subprocess.check_output(cmd, shell=True)
    except subprocess.CalledProcessError:
        logger.error('Error executing {}'.format(cmd))
        return None
    else:
        return out.decode().strip()


class SlurmExecutionServer(object):

    def exit(self):
        pass

    def submit(self, script, id, cores=None, mem=16):

        out_file = "{}.out".format(script)
        err_file = "{}.err".format(script)
        cmd = "sbatch --parsable -N 1 -n 1 -c {} --mem {}G -o {} -e {} {}".format(cores, mem, out_file, err_file, script)

        return execute_command(cmd)

    def status(self, job_id):

        data = defaultdict(dict)

        if job_id is None:
            return "Error"

        cmd = "sacct --parsable2 --format jobid,state --jobs {}".format(job_id)
        out = execute_command(cmd)

        if out is None:
            return "Error"

        lines = out.splitlines()
        for line in csv.DictReader(lines, delimiter='|'):
            id = line.pop('JobID')
            job_id = id.split('.')[0]
            if id == job_id:
                data[job_id] = line
            else:
                data[job_id]['.' + id.split('.')[1]] = line

        return SLURM_STATUS.get(data[job_id]['State'].split(' ')[0], "Unknown")