import datetime
import logging
import requests

logger = logging.getLogger(__file__)

class Task:
    exposed = True

    def __init__(self, server):
        self.server = server

    def GET(self, id=None):
        return self.server.status(job_id=id)

    def POST(self, script=None, id=None, cores=None, mem=16):
        return self.server.submit(script, id, cores, mem)


class Status:
    exposed = True

    def __init__(self, host, port):
        self.default_host = host
        self.default_port = port

    @staticmethod
    def proxy_url(host, port):

        if '/' in host:
            return "http://{}/status".format(host)

        if port == '443':
            return "https://{}/status".format(host)

        if port == '80':
            return "http://{}/status".format(host)

        return "http://{}:{}/status".format(host, port)

    def GET(self, **kwargs):
        url = self.proxy_url(kwargs.get('host', self.default_host), kwargs.get('port', self.default_port))
        return requests.get(url, verify=False, params=kwargs)

    def POST(self, **kwargs):
        return requests.post(self.proxy_url(
                kwargs.get('host', self.default_host),
                kwargs.get('port', self.default_port)
        ), verify=False, data=kwargs)


class Ping:
    exposed = True

    def GET(self):
        return "{}".format(datetime.datetime.now())

    def POST(self):
        return "{}".format(datetime.datetime.now())
