import cherrypy
from configobj import ConfigObj
from validate import Validator
from bgcluster.resources import Task, Status, Ping


def run():

    conf = ConfigObj('../conf/system.conf', configspec='../conf/system.confspec')
    conf.validate(Validator())

    server_conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        }
    }

    if conf['server']['type'] == 'drmaa':
        from bgcluster.schedulers.drmaa import DrmaaExecutionServer
        server = DrmaaExecutionServer(conf['server']['queues'])
    elif conf['server']['type'] == 'slurm':
        from bgcluster.schedulers.slurm import SlurmExecutionServer
        server = SlurmExecutionServer()
    else:
        from bgcluster.schedulers.local import LocalExecutionServer
        server = LocalExecutionServer()

    cherrypy.tree.mount(Task(server), '/api/task', server_conf)
    cherrypy.tree.mount(Ping(), '/api/ping', server_conf)
    cherrypy.tree.mount(Status(conf['web']['host'], conf['web']['port']), '/api/status', server_conf)

    cherrypy.config.update({'server.socket_port': int(conf['server']['port'])})
    cherrypy.config.update({'server.socket_host': conf['server']['listen_ip']})
    cherrypy.engine.start()
    cherrypy.engine.block()
    server.exit()

if __name__ == '__main__':
    run()

